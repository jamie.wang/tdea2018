/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.streamgeeks.ak03hw;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.record.TimestampType;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.*;

/**
 * Tips:
 * 15題是練習GroupBy Date的概念。做法與ak02的q14很像,利用一個“Map(key是date_code, value是record count)"
 */
public class Homework_q15 {

    private static String KAFKA_BROKER_URL = "192.168.1.2:9092"; // 設定要連接的Kafka群
    private static String WORKSHOP_ID = "03";
    private static String STUDENT_ID = "sgxxxx"; // *** <-- 修改成你/妳的學生編號

    public static void main(String[] args) throws Exception {
        // 步驟1. 取得Kafka的Consumer實例
        final Consumer<String, String> consumer = getKafkaConsumer();
        // 步驟2: 設定要訂閱的topic名稱
        String topicName = "ak03.ws" + WORKSHOP_ID + ".hw2." + STUDENT_ID;
        // 步驟3. 讓Consumer向Kafka集群訂閱指定的topic
        consumer.subscribe(Arrays.asList(topicName), new SeekToListener(consumer));

        int recordCounter = 0; // 用來累積收到的record數
        // 產生一個Map: key是日期的字串, value是record count <---- 存放 “題目#15” 答案的容器
        Map<String, Integer> date_RecordCount = new HashMap<>();
        // 一個用來讀取JSON String的物件 <-- 重要
        ObjectMapper om = new ObjectMapper();
        // 一個用來將Date的文字字串轉換成Date物件的格式
        SimpleDateFormat dateStringFormat = new SimpleDateFormat("yyyy/MM/dd");

        try {
            System.out.println("Start listen incoming messages ...");
            // 步驟4. 持續的拉取Kafka進來的訊息
            while (true) {
                // 請求Kafka把新的訊息吐出來
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));
                // 如果有任何新的訊息就會進到下面的迭代
                for (ConsumerRecord<String, String> record : records){
                    recordCounter++;

                    // ** 在這裡進行商業邏輯與訊息處理 **
                    // 取出相關的metadata
                    String topic = record.topic();
                    int partition = record.partition();
                    long offset = record.offset();
                    TimestampType timestampType = record.timestampType();
                    long timestamp = record.timestamp();

                    // 取出msgKey與msgValue
                    String msgKey = record.key();
                    String msgValue = record.value();

                    // ** 根據Spec解析msgValue
                    JsonNode taxidata = om.readTree(msgValue); //<-- 把JSON字串轉換成JsonNode的物件實例

                    // 根據題義取出以下3個欄位vendor_id, payment_type及fare_amount
                    Long pickup_datetime = taxidata.get("pickup_datetime").asLong(); // 因為JSON並沒有定義日期, 要自己轉換
                    Date pickup_date = new Date(pickup_datetime); // 把epoch (1970/1/1 00:00:00)至今的總milli-secs轉換成Date物件
                    String date_key = dateStringFormat.format(pickup_date);

                    // ** “題目#15” 的主要邏輯 **
                    // 把“時間”與“計數"放進一個Map(key是date_key, value是record count)的容器中

                    // *** 你要開發的Block在這裡 [Start] ***

                    // Tips: 在Java中把(key,value)放進"Map"的collection, 使用的method是 map_object.put(key, value)
                    //       把某個key的value從map物件取出來所使用的method是 map_object.get(key)

                    // ...
                    // ...

                    // *** 你要開發的Block在這裡 [End] ***
                }

                // 打印累積讀取筆數與最更新的結果
                System.out.println("Total received records: " + recordCounter);
                System.out.println(mapToString(date_RecordCount));

                // ** 觀察產生的"Accm. Record count"的數值, 如果沒有持續增加就代表
                //    程式己經把現在topic裡有的訊息全部都收進來並且你看到的結果就是問題的答案 **
            }
        } finally {
            // 關掉Producer實例的連線
            consumer.close();
            System.out.println("Stop listen incoming messages");
        }
    }

    // 產生一個Kafka的Consumer物件的實例
    private static Consumer<String, String> getKafkaConsumer() {
        // 步驟1. 設定要連線到Kafka集群的相關設定
        Properties props = new Properties();
        props.put("bootstrap.servers", KAFKA_BROKER_URL); // Kafka集群在那裡?
        props.put("group.id", STUDENT_ID); // <-- 這就是ConsumerGroup
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer"); // 指定msgKey的反序列化器
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer"); // 指定msgValue的反序列化器
        props.put("auto.offset.reset", "earliest"); // 是否從這個ConsumerGroup尚未讀取的partition/offset開始讀
        // 步驟2. 產生一個Kafka的Consumer的實例
        Consumer<String, String> consumer = new KafkaConsumer<>(props);
        return consumer;
    }

    // 一個簡單用來對Map容器進行Key值排序的函式
    private static List<String> sortingMap(Map<String, Integer> datas) {
        List<String> sorted_result = new ArrayList<>(datas.keySet());
        Collections.sort(sorted_result);
        return sorted_result;
    }

    // 一個簡單用來對Map容器進行Key值排序來打印其內容的函式
    private static String mapToString(Map<String, Integer> datas) {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        List<String> sortedKeys = sortingMap(datas);
        int elements_count = 0;
        for(String key : sortingMap(datas)){
            elements_count++;

            sb.append(key);
            sb.append("=");
            sb.append(datas.get(key));

            if(elements_count!=sortedKeys.size())
                sb.append(", ");
        }
        sb.append("}");
        return sb.toString();
    }
}
