/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.streamgeeks.ak03hw;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.record.TimestampType;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;

/**
 * Tips:
 * 跟13題很像, 都是利用簡單的判斷來得到某種觀測值的”最大值"與"最小值"。只不過這此比對大小的條件是"Datetime"。
 */
public class Homework_q14 {

    private static String KAFKA_BROKER_URL = "192.168.1.2:9092"; // 設定要連接的Kafka群
    private static String WORKSHOP_ID = "03";
    private static String STUDENT_ID = "sgxxxx"; // *** <-- 修改成你/妳的學生編號

    public static void main(String[] args) throws Exception {
        // 步驟2. 取得Kafka的Consumer實例
        final Consumer<String, String> consumer = getKafkaConsumer();
        // 步驟3: 設定要訂閱的topic名稱
        String topicName = "ak03.ws" + WORKSHOP_ID + ".hw2." + STUDENT_ID;
        // 步驟4. 讓Consumer向Kafka集群訂閱指定的topic
        consumer.subscribe(Arrays.asList(topicName), new SeekToListener(consumer));
        // 步驟5. 持續的拉取Kafka進來的訊息
        int recordCounter = 0; // 用來累積收到的record數

        // 用來記錄最大與最小的"pickup_datetime"
        Date min_pickup_datetime = null;
        String min_row_number = "";

        Date max_pickup_datetime = null;
        String max_row_number = "";

        // 一個用來讀取JSON String的物件 <-- 重要
        ObjectMapper om = new ObjectMapper();

        // 一個用來將Date的文字字串轉換成Date物件的格式
        SimpleDateFormat dateStringFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");

        try {
            System.out.println("Start listen incoming messages ...");
            while (true) {
                // 請求Kafka把新的訊息吐出來
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));
                // 如果有任何新的訊息就會進到下面的迭代
                for (ConsumerRecord<String, String> record : records){
                    recordCounter++;

                    // ** 在這裡進行商業邏輯與訊息處理 **
                    // 取出相關的metadata
                    String topic = record.topic();
                    int partition = record.partition();
                    long offset = record.offset();
                    TimestampType timestampType = record.timestampType();
                    long timestamp = record.timestamp();

                    // 取出msgKey與msgValue
                    String msgKey = record.key();
                    String msgValue = record.value(); // Taxidata的JSON字串

                    // ** 根據Spec解析msgValue
                    JsonNode taxidata = om.readTree(msgValue); //<-- 把JSON字串轉換成JsonNode的物件實例

                    // 根據題義取出以下2個欄位pickup_datetime, payment_type及fare_amount
                    Long pickup_datetime = taxidata.get("pickup_datetime").asLong(); // 因為JSON並沒有定義日期, 要自己轉換
                    Date pickup_date = new Date(pickup_datetime); // 把epoch (1970/1/1 00:00:00)至今的總milli-secs轉換成Date物件

                    // *** 你要開發的Block在這裡 [Start] ***

                    // Tips: 比較pickup_date與min_pickup_datetime及比較fare_amount與max_pickup_datetime
                    //       把最早與最晚的值保留下來

                    // Tips: 在Java中, 兩個Date物件時間早或晚的比較可以使用 date_object.before(another_date_object)
                    //       或 date_object.after(another_date_object)

                    // ...
                    // ...

                    // *** 你要開發的Block在這裡 [End] ***
                }

                // 打印累積讀取筆數與最更新的結果
                System.out.println("Total received records: " + recordCounter);
                if(min_pickup_datetime!=null)
                    System.out.println("pickup_datetime-Min: " + min_row_number + ":" + dateStringFormat.format(min_pickup_datetime));

                if(max_pickup_datetime!=null)
                    System.out.println("pickup_datetime-Max: " + max_row_number + ":" + dateStringFormat.format(max_pickup_datetime));

                // ** 觀察產生的"Accm. Record count"的數值, 如果沒有持續增加就代表
                //    程式己經把現在topic裡有的訊息全部都收進來並且你看到的結果就是問題的答案 **
            }
        } finally {
            // 關掉Producer實例的連線
            consumer.close();
            System.out.println("Stop listen incoming messages");
        }
    }

    // 產生一個Kafka的Consumer物件的實例
    private static Consumer<String, String> getKafkaConsumer() {
        // 步驟1. 設定要連線到Kafka集群的相關設定
        Properties props = new Properties();
        props.put("bootstrap.servers", KAFKA_BROKER_URL); // Kafka集群在那裡?
        props.put("group.id", STUDENT_ID); // <-- 這就是ConsumerGroup
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer"); // 指定msgKey的反序列化器
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer"); // 指定msgValue的反序列化器
        props.put("auto.offset.reset", "earliest"); // 是否從這個ConsumerGroup尚未讀取的partition/offset開始讀
        // 步驟2. 產生一個Kafka的Consumer的實例
        Consumer<String, String> consumer = new KafkaConsumer<>(props);
        return consumer;
    }
}
