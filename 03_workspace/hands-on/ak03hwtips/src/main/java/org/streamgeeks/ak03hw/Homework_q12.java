/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.streamgeeks.ak03hw;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.record.TimestampType;
import org.streamgeeks.ak03hw.model.Taxidata;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.*;

/**
 * Tips:
 * 由於12題主要是要練習資料的解析、欄位資料型別的轉換(尤其是日期-timestamp與數子-decimal）再加上將物件(Object)序列化到Kafka的過程。
 * 在資料處理的pipeline（ETL）中, 資料的轉換（序列化/反序列化及資料型別的解析與轉換)是不可避免的苦工, 但卻對後面的應用或分析很關鍵。
 * 這個程式只能跑一次, 不然資料會重覆。如果有問題, 則需要使用kafka-topic的command來移除topic再重拋!
 */
public class Homework_q12 {

    private static String KAFKA_BROKER_URL = "localhost:9092"; // 設定要連接的Kafka群
    private static String WORKSHOP_ID = "03";
    private static String STUDENT_ID = "sg0056"; // *** <-- 修改成你/妳的學生編號

    public static void main(String[] args) throws Exception {
        // 步驟1. 取得Kafka的Producer實例
        final Producer<String, String> producer = getKafkaProducer();
        // 步驟2. 取得Kafka的Consumer實例
        final Consumer<String, String> consumer = getKafkaConsumer();

        // 步驟3: 設定要訂閱的topic名稱
        String topicName_Sub = "ak03.ws" + WORKSHOP_ID + ".hw1." + STUDENT_ID;

        // 步驟: 設定要發佈的topic名稱
        String topicName_Pub = "ak03.ws" + WORKSHOP_ID + ".hw2." + STUDENT_ID;

        // 步驟4. 讓Consumer向Kafka集群訂閱指定的topic
        consumer.subscribe(Arrays.asList(topicName_Sub), new SeekToListener(consumer));

        // 步驟5. 持續的拉取Kafka進來的訊息
        int recordCounter = 0; // 用來累積收到的record數
        int validRecordCount = 0; // 用來計算有效的資料筆數
        int invalidRecordCount = 0; // 用來計算無效的資料筆數

        // 一個用來計錄那些row_number為不符合規定的容器 (key: 是row number, value: 是error message)
        Map<String, String> invalid_records = new HashMap<>();

        // 一個用來將DTO物件轉換成(JSON String)的物件 <-- 重要
        ObjectMapper om = new ObjectMapper();

        // 一個用來將Date的文字字串轉換成Date物件的格式
        SimpleDateFormat dateStringFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        try {
            System.out.println("Start listen incoming messages ...");

            while (true) {
                // 請求Kafka把新的訊息吐出來
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));
                // 如果有任何新的訊息就會進到下面的迭代
                for (ConsumerRecord<String, String> record : records){
                    recordCounter++;
                    // ** 在這裡進行商業邏輯與訊息處理 **
                    // 取出相關的metadata
                    String topic = record.topic();
                    int partition = record.partition();
                    long offset = record.offset();
                    TimestampType timestampType = record.timestampType();
                    long timestamp = record.timestamp();
                    // 取出msgKey與msgValue
                    String msgKey = record.key();
                    String msgValue = record.value();

                    // ** 根據Spec解析msgValue

                    // 檢查資料是否有值且不能空白
                    if (msgValue==null || msgKey.isEmpty()) {
                        // 記錄
                        invalidRecordCount++;
                        invalid_records.put(msgKey, "Record is either null or empty");
                        continue;
                    }

                    // 解析內容 (應該要有21個欄位值
                    String[] elements = msgValue.split(",");

                    if (elements.length<20 || elements.length>21){
                        // 記錄
                        invalidRecordCount++;
                        invalid_records.put(msgKey, "Record elements size is equal to 21");
                        continue;
                    }

                    // 進行每個欄位的DataType的轉換與解析
                    Taxidata taxidata = null;
                    try{
                        String row_id = msgKey;
                        String medallion = elements[0];
                        String hack_license = elements[1];
                        String vendor_id = elements[2];
                        String payment_type = elements[3];
                        Float fare_amount = Float.parseFloat(elements[4]);
                        Float surcharge = Float.parseFloat(elements[5]);
                        Float mta_tax = Float.parseFloat(elements[6]);
                        Float tip_amount = Float.parseFloat(elements[7]);
                        Float tolls_amount = Float.parseFloat(elements[8]);
                        Float total_amount = Float.parseFloat(elements[9]);
                        Integer rate_code = Integer.parseInt(elements[10]);
                        Date pickup_datetime = dateStringFormat.parse(elements[11]);
                        Date dropoff_datetime = dateStringFormat.parse(elements[12]);
                        Integer passenger_count = Integer.parseInt(elements[13]);
                        Integer trip_time_in_secs = Integer.parseInt(elements[14]);
                        Float trip_distance = Float.parseFloat(elements[15]);
                        Float pickup_longitude = Float.parseFloat(elements[16]);
                        Float pickup_latitude = Float.parseFloat(elements[17]);
                        Float dropoff_longitude = Float.parseFloat(elements[18]);
                        Float dropoff_latitude = Float.parseFloat(elements[19]);
                        String credit_card = "";

                        if (elements.length==21)
                            credit_card = elements[20];

                        // 建立Taxidata的實例
                        taxidata = new Taxidata(row_id, medallion, hack_license, vendor_id, payment_type,
                                fare_amount, surcharge, mta_tax, tip_amount, tolls_amount, total_amount, rate_code,
                                pickup_datetime, dropoff_datetime, passenger_count, trip_time_in_secs, trip_distance,
                                pickup_longitude, pickup_latitude, dropoff_longitude, dropoff_latitude, credit_card);

                    }catch (Exception e){
                        // 記錄
                        invalidRecordCount++;
                        invalid_records.put(msgKey, e.getMessage());
                        continue;
                    }

                    // 秀出metadata與msgKey & msgValue訊息
                    // System.out.println(topic + "-" + partition + "-" + offset + " : (" + record.key() + ", " + record.value() + ")");
                    System.out.println(om.writeValueAsString(taxidata));

                    // 發佈訊息到新的Topic
                    producer.send(new ProducerRecord<>(topicName_Pub, msgKey, om.writeValueAsString(taxidata)));
                    validRecordCount++;
                }

                // 打印累積讀取筆數與最更新的結果
                System.out.println("Total received records: " + recordCounter);
                System.out.println("Total Valid records: " + validRecordCount);
                System.out.println("Total Invalid records: " + invalidRecordCount);
                System.out.println("Invalid record numbers: " + mapToString(invalid_records));

                // ** 觀察產生的"Accm. Record count"的數值, 如果沒有持續增加就代表
                //    程式己經把現在topic裡有的訊息全部都收進來並且你看到的結果就是問題的答案 **
            }
        } finally {
            // 關掉Producer實例的連線
            producer.close();
            consumer.close();
            System.out.println("Stop listen incoming messages");
        }
    }

    // 產生一個Kafka的Producer物件的實例
    private static Producer<String, String> getKafkaProducer() {
        // 步驟1. 設定要連線到Kafka集群的相關設定
        Properties props = new Properties();
        props.put("bootstrap.servers", KAFKA_BROKER_URL); // Kafka集群在那裡?
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer"); // 指定msgKey的序列化器
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer"); // 指定msgValue的序列化器
        props.put("acks","all");
        props.put("max.in.flight.requests.per.connection","1");
        props.put("retries",Integer.MAX_VALUE+"");
        // 步驟2. 產生一個Kafka的Producer的實例
        Producer<String, String> producer = new KafkaProducer<>(props);
        return producer;
    }

    // 產生一個Kafka的Consumer物件的實例
    private static Consumer<String, String> getKafkaConsumer() {
        // 步驟1. 設定要連線到Kafka集群的相關設定
        Properties props = new Properties();
        props.put("bootstrap.servers", KAFKA_BROKER_URL); // Kafka集群在那裡?
        props.put("group.id", STUDENT_ID); // <-- 這就是ConsumerGroup
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer"); // 指定msgKey的反序列化器
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer"); // 指定msgValue的反序列化器
        props.put("auto.offset.reset", "earliest"); // 是否從這個ConsumerGroup尚未讀取的partition/offset開始讀
        // 步驟2. 產生一個Kafka的Consumer的實例
        Consumer<String, String> consumer = new KafkaConsumer<>(props);
        return consumer;
    }

    // 一個簡單用來對Map容器進行Key值排序的函式
    private static List<String> sortingMap(Map<String, String> datas) {
        List<String> sorted_result = new ArrayList<>(datas.keySet());
        Collections.sort(sorted_result);
        return sorted_result;
    }

    // 一個簡單用來對Map容器進行Key值排序來打印其內容的函式
    private static String mapToString(Map<String, String> datas) {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        List<String> sortedKeys = sortingMap(datas);
        int elements_count = 0;
        for(String key : sortingMap(datas)){
            elements_count++;
            sb.append(key);
            sb.append("=");
            sb.append(datas.get(key));
            if(elements_count!=sortedKeys.size())
                sb.append(", ");
        }
        sb.append("}");
        return sb.toString();
    }
}
