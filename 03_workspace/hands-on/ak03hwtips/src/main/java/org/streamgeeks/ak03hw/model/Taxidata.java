/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.streamgeeks.ak03hw.model;

import java.util.Date;
import java.util.Objects;

/**
 * 一個DTO（Data Transfer Object)的示範
 */
public class Taxidata {
    private String row_id;
    private String medallion;
    private String hack_license;
    private String vendor_id;
    private String payment_type;
    private Float fare_amount = 0.0f;
    private Float surcharge = 0.0f;
    private Float mta_tax = 0.0f;
    private Float tip_amount = 0.0f;
    private Float tolls_amount = 0.0f;
    private Float total_amount = 0.0f;
    private Integer rate_code = 0;
    private Date pickup_datetime;
    private Date dropoff_datetime;
    private Integer passenger_count = 0;
    private Integer trip_time_in_secs = 0;
    private Float trip_distance = 0.0f;
    private Float pickup_longitude = 0.0f;
    private Float pickup_latitude = 0.0f;
    private Float dropoff_longitude = 0.0f;
    private Float dropoff_latitude = 0.0f;
    private String credit_card;

    public Taxidata() {
    }

    public Taxidata(String row_id, String medallion, String hack_license, String vendor_id, String payment_type, Float fare_amount, Float surcharge, Float mta_tax, Float tip_amount, Float tolls_amount, Float total_amount, Integer rate_code, Date pickup_datetime, Date dropoff_datetime, Integer passenger_count, Integer trip_time_in_secs, Float trip_distance, Float pickup_longitude, Float pickup_latitude, Float dropoff_longitude, Float dropoff_latitude, String credit_card) {
        this.row_id = row_id;
        this.medallion = medallion;
        this.hack_license = hack_license;
        this.vendor_id = vendor_id;
        this.payment_type = payment_type;
        this.fare_amount = fare_amount;
        this.surcharge = surcharge;
        this.mta_tax = mta_tax;
        this.tip_amount = tip_amount;
        this.tolls_amount = tolls_amount;
        this.total_amount = total_amount;
        this.rate_code = rate_code;
        this.pickup_datetime = pickup_datetime;
        this.dropoff_datetime = dropoff_datetime;
        this.passenger_count = passenger_count;
        this.trip_time_in_secs = trip_time_in_secs;
        this.trip_distance = trip_distance;
        this.pickup_longitude = pickup_longitude;
        this.pickup_latitude = pickup_latitude;
        this.dropoff_longitude = dropoff_longitude;
        this.dropoff_latitude = dropoff_latitude;
        this.credit_card = credit_card;
    }

    public String getRow_id() {
        return row_id;
    }

    public void setRow_id(String row_id) {
        this.row_id = row_id;
    }

    public String getMedallion() {
        return medallion;
    }

    public void setMedallion(String medallion) {
        this.medallion = medallion;
    }

    public String getHack_license() {
        return hack_license;
    }

    public void setHack_license(String hack_license) {
        this.hack_license = hack_license;
    }

    public String getVendor_id() {
        return vendor_id;
    }

    public void setVendor_id(String vendor_id) {
        this.vendor_id = vendor_id;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public Float getFare_amount() {
        return fare_amount;
    }

    public void setFare_amount(Float fare_amount) {
        this.fare_amount = fare_amount;
    }

    public Float getSurcharge() {
        return surcharge;
    }

    public void setSurcharge(Float surcharge) {
        this.surcharge = surcharge;
    }

    public Float getMta_tax() {
        return mta_tax;
    }

    public void setMta_tax(Float mta_tax) {
        this.mta_tax = mta_tax;
    }

    public Float getTip_amount() {
        return tip_amount;
    }

    public void setTip_amount(Float tip_amount) {
        this.tip_amount = tip_amount;
    }

    public Float getTolls_amount() {
        return tolls_amount;
    }

    public void setTolls_amount(Float tolls_amount) {
        this.tolls_amount = tolls_amount;
    }

    public Float getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(Float total_amount) {
        this.total_amount = total_amount;
    }

    public Integer getRate_code() {
        return rate_code;
    }

    public void setRate_code(Integer rate_code) {
        this.rate_code = rate_code;
    }

    public Date getPickup_datetime() {
        return pickup_datetime;
    }

    public void setPickup_datetime(Date pickup_datetime) {
        this.pickup_datetime = pickup_datetime;
    }

    public Date getDropoff_datetime() {
        return dropoff_datetime;
    }

    public void setDropoff_datetime(Date dropoff_datetime) {
        this.dropoff_datetime = dropoff_datetime;
    }

    public Integer getPassenger_count() {
        return passenger_count;
    }

    public void setPassenger_count(Integer passenger_count) {
        this.passenger_count = passenger_count;
    }

    public Integer getTrip_time_in_secs() {
        return trip_time_in_secs;
    }

    public void setTrip_time_in_secs(Integer trip_time_in_secs) {
        this.trip_time_in_secs = trip_time_in_secs;
    }

    public Float getTrip_distance() {
        return trip_distance;
    }

    public void setTrip_distance(Float trip_distance) {
        this.trip_distance = trip_distance;
    }

    public Float getPickup_longitude() {
        return pickup_longitude;
    }

    public void setPickup_longitude(Float pickup_longitude) {
        this.pickup_longitude = pickup_longitude;
    }

    public Float getPickup_latitude() {
        return pickup_latitude;
    }

    public void setPickup_latitude(Float pickup_latitude) {
        this.pickup_latitude = pickup_latitude;
    }

    public Float getDropoff_longitude() {
        return dropoff_longitude;
    }

    public void setDropoff_longitude(Float dropoff_longitude) {
        this.dropoff_longitude = dropoff_longitude;
    }

    public Float getDropoff_latitude() {
        return dropoff_latitude;
    }

    public void setDropoff_latitude(Float dropoff_latitude) {
        this.dropoff_latitude = dropoff_latitude;
    }

    public String getCredit_card() {
        return credit_card;
    }

    public void setCredit_card(String credit_card) {
        this.credit_card = credit_card;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Taxidata taxidata = (Taxidata) o;
        return Objects.equals(row_id, taxidata.row_id) &&
                Objects.equals(medallion, taxidata.medallion) &&
                Objects.equals(hack_license, taxidata.hack_license) &&
                Objects.equals(vendor_id, taxidata.vendor_id) &&
                Objects.equals(payment_type, taxidata.payment_type) &&
                Objects.equals(fare_amount, taxidata.fare_amount) &&
                Objects.equals(surcharge, taxidata.surcharge) &&
                Objects.equals(mta_tax, taxidata.mta_tax) &&
                Objects.equals(tip_amount, taxidata.tip_amount) &&
                Objects.equals(tolls_amount, taxidata.tolls_amount) &&
                Objects.equals(total_amount, taxidata.total_amount) &&
                Objects.equals(rate_code, taxidata.rate_code) &&
                Objects.equals(pickup_datetime, taxidata.pickup_datetime) &&
                Objects.equals(dropoff_datetime, taxidata.dropoff_datetime) &&
                Objects.equals(passenger_count, taxidata.passenger_count) &&
                Objects.equals(trip_time_in_secs, taxidata.trip_time_in_secs) &&
                Objects.equals(trip_distance, taxidata.trip_distance) &&
                Objects.equals(pickup_longitude, taxidata.pickup_longitude) &&
                Objects.equals(pickup_latitude, taxidata.pickup_latitude) &&
                Objects.equals(dropoff_longitude, taxidata.dropoff_longitude) &&
                Objects.equals(dropoff_latitude, taxidata.dropoff_latitude) &&
                Objects.equals(credit_card, taxidata.credit_card);
    }

    @Override
    public int hashCode() {

        return Objects.hash(row_id, medallion, hack_license, vendor_id, payment_type, fare_amount, surcharge, mta_tax, tip_amount, tolls_amount, total_amount, rate_code, pickup_datetime, dropoff_datetime, passenger_count, trip_time_in_secs, trip_distance, pickup_longitude, pickup_latitude, dropoff_longitude, dropoff_latitude, credit_card);
    }

    @Override
    public String toString() {
        return "Taxidata{" +
                "row_id=" + row_id +
                ", medallion='" + medallion + '\'' +
                ", hack_license='" + hack_license + '\'' +
                ", vendor_id='" + vendor_id + '\'' +
                ", payment_type='" + payment_type + '\'' +
                ", fare_amount=" + fare_amount +
                ", surcharge=" + surcharge +
                ", mta_tax=" + mta_tax +
                ", tip_amount=" + tip_amount +
                ", tolls_amount=" + tolls_amount +
                ", total_amount=" + total_amount +
                ", rate_code=" + rate_code +
                ", pickup_datetime=" + pickup_datetime +
                ", dropoff_datetime=" + dropoff_datetime +
                ", passenger_count=" + passenger_count +
                ", trip_time_in_secs=" + trip_time_in_secs +
                ", trip_distance=" + trip_distance +
                ", pickup_longitude=" + pickup_longitude +
                ", pickup_latitude=" + pickup_latitude +
                ", dropoff_longitude=" + dropoff_longitude +
                ", dropoff_latitude=" + dropoff_latitude +
                ", credit_card='" + credit_card + '\'' +
                '}';
    }
}
