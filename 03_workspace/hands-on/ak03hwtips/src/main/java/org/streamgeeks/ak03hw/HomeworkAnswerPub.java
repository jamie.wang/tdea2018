/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.streamgeeks.ak03hw;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;

/**
 * 用來發佈ak03作業的範例程式
 */
public class HomeworkAnswerPub {

    private static String KAFKA_BROKER_URL = "192.168.1.2:9092"; // 設定要連接的Kafka群
    private static String WORKSHOP_ID = "03";
    private static String STUDENT_ID = "sg0056"; // *** <-- 修改成你/妳的學生編號

    public static void main(String[] args) {
        // 步驟1. 設定要連線到Kafka集群的相關設定
        Properties props = new Properties();
        // Kafka集群在那裡? (在Workshop中負責評分的Kafka)
        props.put("bootstrap.servers", KAFKA_BROKER_URL); // <-- 置換成要連接的Kafka群
        // 指定msgKey的序列化器
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        // 指定msgValue的序列化器
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        // 讓producer降低資料不見、亂序或重覆機率的設定
        props.put("acks","all");
        props.put("max.in.flight.requests.per.connection","1");
        props.put("retries",Integer.MAX_VALUE+"");

        // 步驟2. 產生一個Kafka的Producer的實例
        Producer<String, String> producer = new KafkaProducer<>(props);
        // 步驟3. 指定想要發佈訊息的topic名稱
        String topicName = "ak03.ws"+WORKSHOP_ID+".homework"; // 個人作業繳交的Topic

        int msgCounter = 15; // ak03的作業總共有15題

        try {
            System.out.println("Start sending messages ...");
            // 步驟4. 產生要發佈到Kafka的訊息 (把訊息封裝進一個ProducerRecord的實例中)
            //    - 參數#1: topicName, 參數#2: msgKey, 參數#3: msgValue

            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|1",  "1"));  // 第1題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|2",  "2"));  // 第2題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|3",  "3"));  // 第3題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|4",  "5"));  // 第4題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|5",  "4"));  // 第5題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|6",  "3"));  // 第6題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|7",  "2"));  // 第7題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|8",  "2"));  // 第8題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|9",  "4"));  // 第9題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|10", "5")); // 第10題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|11", "5386")); // 第11題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|12", "156")); // 第12題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|13", "0")); // 第13題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|14", "0")); // 第14題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|15", "0")); // 第15題

            System.out.println("Submit " + msgCounter + " answers for ak03 Homework");
        } catch (Exception e) {
            // 錯誤處理
            e.printStackTrace();
        }
        // 步驟5. 關掉Producer實例的連線
        producer.close();
        System.out.println("Message sending completed!");
    }
}
