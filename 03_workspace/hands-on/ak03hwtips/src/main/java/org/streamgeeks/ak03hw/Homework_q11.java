/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.streamgeeks.ak03hw;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * Tips:
 * 由於11題主要是要練習Row(行)的方式讀取CSV並將之拋轉進Kafka, 目的只是為了產生q12所需要的event data。
 * 這個程式只能跑一次, 不然資料會重覆。如果有問題, 則需要使用kafka-topic的command來移除topic再重拋!
 */
public class Homework_q11 {

    private static String KAFKA_BROKER_URL = "localhost:9092"; // 設定要連接的Kafka群
    private static String WORKSHOP_ID = "03";
    private static String STUDENT_ID = "sg0056"; // *** <-- 修改成你/妳的學生編號

    public static void main(String[] args) throws Exception {
        // 步驟1. 取得Kafka的Producer實例
        final Producer<String, String> producer = getKafkaProducer();

        // 步驟2: 設定要發佈的topic名稱
        String topicName = "ak03.ws" + WORKSHOP_ID + ".hw1." + STUDENT_ID;

        // 步驟3: 讀取讀取CSV的檔案(注意: nyc_taxi_data.csv必需要在專案的根目錄
        String csv_file = "nyc_taxi_data.csv";
        int validRecordCount = 0; // 用來計算有效的資料筆數
        int invalidRecordCount = 0; // 用來計算無效的資料筆數
        int rowNumber = 0; // 用來

        // 如何逐行讀取CSV的每一行資料
        try {
            File csv_data = new File(csv_file);
            BufferedReader csv_data_buffer = new BufferedReader(new FileReader(csv_data));
            String lineContent = "";

            // 迭代讀取每一行的資料
            while((lineContent = csv_data_buffer.readLine()) != null) {
                rowNumber++;
                if (rowNumber == 1)
                    continue; // 由於第一行是header, 所以我們要ignore

                // 進行每一行的資料檢查或相對應的處理
                if (lineContent==null || lineContent.isEmpty())
                    invalidRecordCount++;
                else {
                    // 這是一個有效的資料
                    validRecordCount++;
                    // System.out.println(rowNumber-1 + "," + lineContent);
                    RecordMetadata recordMetadata = producer.send(new ProducerRecord<>(topicName, ""+(rowNumber-1), lineContent)).get(); // 讓我們使用sync的手法來看每一筆的回應

                    System.out.println("Topic:Partition:Offset: [" + recordMetadata.topic() + "]:["
                            + recordMetadata.partition() + "]:["
                            + recordMetadata.offset() + "] -- Row Number: " + (rowNumber-1));
                }
            }
        } catch(IOException e) {
            e.printStackTrace(); // 有出現問題
        }
        System.out.println("Total CSV records: " + (rowNumber-1));
        System.out.println("Total Valid records: " + validRecordCount);
        System.out.println("Total Invalid records: " + invalidRecordCount);

        // 步驟4. 關掉Producer實例的連線
        producer.close();
        System.out.println("Message sending completed!");
    }

    // 產生一個Kafka的Producer物件的實例
    private static Producer<String, String> getKafkaProducer() {
        // 步驟1. 設定要連線到Kafka集群的相關設定
        Properties props = new Properties();
        props.put("bootstrap.servers", KAFKA_BROKER_URL); // Kafka集群在那裡?
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer"); // 指定msgKey的序列化器
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer"); // 指定msgValue的序列化器
        props.put("acks","all");
        props.put("max.in.flight.requests.per.connection","1");
        props.put("retries",Integer.MAX_VALUE+"");
        // 步驟2. 產生一個Kafka的Producer的實例
        Producer<String, String> producer = new KafkaProducer<>(props);
        return producer;
    }
}
