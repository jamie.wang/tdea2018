/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.streamgeeks.ak02hw;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;

/**
 * 用來發佈ak02作業的範例程式
 */
public class HomeworkAnswerPub {

    private static String KAFKA_BROKER_URL = "192.168.1.2:9092"; // 設定要連接的Kafka群
    private static String WORKSHOP_ID = "03";
    private static String STUDENT_ID = "sg0056"; // *** <-- 修改成你/妳的學生編號

    public static void main(String[] args) {
        // 步驟1. 設定要連線到Kafka集群的相關設定
        Properties props = new Properties();
        // Kafka集群在那裡? (在Workshop中負責評分的Kafka)
        props.put("bootstrap.servers", KAFKA_BROKER_URL); // <-- 置換成要連接的Kafka群
        // 指定msgKey的序列化器
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        // 指定msgValue的序列化器
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        // 步驟2. 產生一個Kafka的Producer的實例
        Producer<String, String> producer = new KafkaProducer<>(props);
        // 步驟3. 指定想要發佈訊息的topic名稱
        String topicName = "ak02.ws" + WORKSHOP_ID + ".homework"; // 個人作業繳交的Topic

        int msgCounter = 20; // ak01的作業總共有20題
        try {
            System.out.println("Start sending messages ...");
            // 步驟4. 產生要發佈到Kafka的訊息 (把訊息封裝進一個ProducerRecord的實例中)
            //    - 參數#1: topicName, 參數#2: msgKey, 參數#3: msgValue

            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|1",  "5"));  // 第1題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|2",  "1"));  // 第2題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|3",  "1"));  // 第3題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|4",  "2"));  // 第4題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|5",  "2"));  // 第5題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|6",  "3"));  // 第6題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|7",  "1"));  // 第7題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|8",  "4"));  // 第8題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|9",  "5"));  // 第9題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|10", "3")); // 第10題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|11", "1")); // 第11題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|12", "2")); // 第12題

            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|13", "part_01|part_02|part_03|part_04|part_05|part_06|part_07|part_08|part_09|part_10")); // 第13題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|14", "part_01=100159|part_02=99845|part_03=99865|part_04=100048|part_05=99724|part_06=99697|part_07=99787|part_08=100529|part_09=100374|part_10=99972")); // 第14題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|15", "part_01=-107934|part_02=100756|part_03=241595|part_04=-161842|part_05=-161375|part_06=-26140|part_07=66014|part_08=-138518|part_09=55419|part_10=-36821")); // 第15題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|16", "part_01=100159|part_02=99845|part_03=99865|part_04=100048|part_05=99724|part_06=99697|part_07=99787|part_08=100529|part_09=100374|part_10=99972")); // 第16題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|17", "part_01=100159|part_02=99845|part_03=99865|part_04=100048|part_05=99724|part_06=99697|part_07=99787|part_08=100529|part_09=100374|part_10=99972")); // 第17題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|18", "0")); // 第18題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|19", "0")); // 第19題
            producer.send(new ProducerRecord<>(topicName, STUDENT_ID+"|20", "0")); // 第20題

            System.out.println("Submit " + msgCounter + " answers for ak01 Homework");
        } catch (Exception e) {
            // 錯誤處理
            e.printStackTrace();
        }
        // 步驟5. 關掉Producer實例的連線
        producer.close();
        System.out.println("Message sending completed!");
    }
}
