/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.streamgeeks.ak03.consumer;

import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class RebalanceHandler implements ConsumerRebalanceListener {
    private Map<TopicPartition, OffsetAndMetadata> currentOffsets = new HashMap<>();
    private Consumer consumer;

    public RebalanceHandler(Consumer consumer, Map<TopicPartition, OffsetAndMetadata> currentOffsets){
        this.consumer = consumer;
        this.currentOffsets = currentOffsets;
    }

    // 當Rebalance被觸發後, Consumer被取回被assigned的partitions
    @Override
    public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
        System.out.println("Lost partitions in rebalance. Commiting current Offsets: " + currentOffsets);
        consumer.commitSync(currentOffsets);
    }

    // 當Rebalance被觸發後, Consumer被通知有那些partition被assigned
    @Override
    public void onPartitionsAssigned(Collection<TopicPartition> partitions) {

    }


}
