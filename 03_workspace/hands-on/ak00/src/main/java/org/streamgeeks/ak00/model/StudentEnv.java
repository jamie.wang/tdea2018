package org.streamgeeks.ak00.model;

import java.util.Objects;

/**
 * 用來檢查學員的個人開發環境:主要是作業系統以及JDK的版本
 */
public class StudentEnv {
    private String studentId = "";
    private String osName= "";
    private String javaRuntime= "";
    private String javaVmVersion= "";
    private String javaVmVendor= "";
    private String javaRuntimeVersion= "";
    private String javaSpecVersion= "";

    public StudentEnv(String studentId, String osName, String javaRuntime, String javaVmVersion, String javaVmVendor, String javaRuntimeVersion, String javaSpecVersion) {
        this.studentId = studentId;
        this.osName = osName;
        this.javaRuntime = javaRuntime;
        this.javaVmVersion = javaVmVersion;
        this.javaVmVendor = javaVmVendor;
        this.javaRuntimeVersion = javaRuntimeVersion;
        this.javaSpecVersion = javaSpecVersion;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getOsName() {
        return osName;
    }

    public void setOsName(String osName) {
        this.osName = osName;
    }

    public String getJavaRuntime() {
        return javaRuntime;
    }

    public void setJavaRuntime(String javaRuntime) {
        this.javaRuntime = javaRuntime;
    }

    public String getJavaVmVersion() {
        return javaVmVersion;
    }

    public void setJavaVmVersion(String javaVmVersion) {
        this.javaVmVersion = javaVmVersion;
    }

    public String getJavaVmVendor() {
        return javaVmVendor;
    }

    public void setJavaVmVendor(String javaVmVendor) {
        this.javaVmVendor = javaVmVendor;
    }

    public String getJavaRuntimeVersion() {
        return javaRuntimeVersion;
    }

    public void setJavaRuntimeVersion(String javaRuntimeVersion) {
        this.javaRuntimeVersion = javaRuntimeVersion;
    }

    public String getJavaSpecVersion() {
        return javaSpecVersion;
    }

    public void setJavaSpecVersion(String javaSpecVersion) {
        this.javaSpecVersion = javaSpecVersion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StudentEnv that = (StudentEnv) o;
        return Objects.equals(studentId, that.studentId) &&
                Objects.equals(osName, that.osName) &&
                Objects.equals(javaRuntime, that.javaRuntime) &&
                Objects.equals(javaVmVersion, that.javaVmVersion) &&
                Objects.equals(javaVmVendor, that.javaVmVendor) &&
                Objects.equals(javaRuntimeVersion, that.javaRuntimeVersion) &&
                Objects.equals(javaSpecVersion, that.javaSpecVersion);
    }

    @Override
    public int hashCode() {

        return Objects.hash(studentId, osName, javaRuntime, javaVmVersion, javaVmVendor, javaRuntimeVersion, javaSpecVersion);
    }

    @Override
    public String toString() {
        return "StudentEnv{" +
                "studentId='" + studentId + '\'' +
                ", osName='" + osName + '\'' +
                ", javaRuntime='" + javaRuntime + '\'' +
                ", javaVmVersion='" + javaVmVersion + '\'' +
                ", javaVmVendor='" + javaVmVendor + '\'' +
                ", javaRuntimeVersion='" + javaRuntimeVersion + '\'' +
                ", javaSpecVersion='" + javaSpecVersion + '\'' +
                '}';
    }
}
