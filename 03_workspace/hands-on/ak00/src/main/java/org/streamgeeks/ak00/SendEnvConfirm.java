/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.streamgeeks.ak00;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.streamgeeks.ak00.model.StudentEnv;

import java.util.Properties;

public class SendEnvConfirm {
    public static void main(String[] args) {
        // 步驟1. 設定要連線到Kafka集群的相關設定
        Properties props = new Properties();
        props.put("bootstrap.servers", "streamgeeks.org:9092"); // Kafka集群在那裡?
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer"); // 指定msgKey的序列化器
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer"); // 指定msgValue的序列化器

        // 用來序列化物件to Json
        ObjectMapper om = new ObjectMapper();

        // 步驟2. 產生一個Kafka的Producer的實例
        Producer<String, String> producer = new KafkaProducer<>(props);

        // 步驟4. 修改學員編號
        String studentId = "{STUDENT_ID}"; // <-- 修改成你/妳的學生編號

        // 步驟3. 指定想要發佈訊息的topic名稱
        String topicName = "ak00.ws03.env";

        try {
            // 檢查學員編號
            if(validateStudentID(studentId)){ // 有效的學員編號
                System.out.println("Start to check environment ....");

                // 取得學員的JDK環境與O.S
                StudentEnv studentEnv = buildStudentEvn(studentId);
                validateStudentEnv(studentEnv);

                // 發佈學員JDK與OS訊息到Kakfa以便助教幫助學員解決環境問題
                //    - 參數#1: topicName
                //    - 參數#2: msgKey
                //    - 參數#3: msgValue
                producer.send(new ProducerRecord<>(topicName, studentId, om.writeValueAsString(studentEnv))).get();

                System.out.println("The confirmation is send out successfully!");
            }
        } catch (Exception e) {
            // 錯誤處理
            e.printStackTrace();

            System.err.println("Encounter issue to send out confirmation!");
        }

        // 步驟5. 關掉Producer實例的連線
        producer.close();
    }

    // 檢查學員編號是否為有效的format
    private static boolean validateStudentID(String studentID) {
        if (studentID==null || studentID.isEmpty()){
            System.err.println("Error: Invalid student ID!");
            return false;
        }

        // 檢查長度
        if (studentID.length()!= 6) {
            System.err.println("Error: Invalid student ID!");
            return false;
        }

        // 檢查前綴碼
        if (!studentID.toLowerCase().startsWith("sg")) {
            System.err.println("Error: Invalid student ID!");
            return false;
        }

        return true;
    }

    // 檢查學員的JDK環境與O.S
    private static StudentEnv buildStudentEvn(String studentID) {
        String studentId = studentID;
        String osName= System.getProperty("java.runtime.name"); // JDK Runtime Environment
        String javaRuntime= System.getProperty("java.vm.version"); // 25.163-b01
        String javaVmVersion= System.getProperty("java.vm.vendor"); // Azul Systems, Inc.
        String javaVmVendor= System.getProperty("java.runtime.version"); // 1.8.0_163-b01
        String javaRuntimeVersion= System.getProperty("os.name"); // Windows 10
        String javaSpecVersion= System.getProperty("java.specification.version"); // 1.8

        StudentEnv studentEnv = new StudentEnv(studentID, osName, javaRuntime, javaVmVersion, javaVmVendor, javaRuntimeVersion, javaSpecVersion);
        return studentEnv;
    }

    private static void validateStudentEnv(StudentEnv env) {
        System.out.println();
        boolean result = true;

        // 檢查JDK版本是否大於等於 1.8
        try{
            float ver = Float.parseFloat(env.getJavaSpecVersion());
            if(ver < 1.8f){
                System.out.println(String.format("Your JDK spec version is {}, the minimum version is {}", env.getJavaSpecVersion(), "1.8"));
                result = false;
            }
        }catch(Exception e){
            result = false;
        }

        if (result) {
            System.out.println("Congratulations, your Environment is ready!");
        } else {
            System.out.println("Your Java Environment is \"NOT\" fulfilled class requirement.");
            System.out.println("Please contact Teaching Assistent (TA) to resolve.");
            System.out.println();
            System.out.println("** The minimum requiremets: OpenJDK & Java ver >= 1.8 **");
        }
    }
}
